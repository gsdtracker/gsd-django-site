from django.urls import path
from . import views as user_views
urlpatterns = [
    path('', user_views.index, name='index'),
    path('login/', user_views.login, name='login'),
    path('logout/', user_views.logout, name='logout'),
    path('register/', user_views.register, name='register'),
    path('overview/', user_views.overview, name='overview'),
    path('reports/', user_views.reports, name='reports'),
    path('alerts/', user_views.alerts, name='alerts'),
    path('manage/', user_views.manage, name='manage'),
    path('search/', user_views.search, name='search'),
    path('settings/', user_views.settings, name='settings'),
    path('chat/', user_views.chat, name='chat'),

]
