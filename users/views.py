from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib import messages

available_trackers=[
['test1','test1','test1'],
['test2','test2','test2'],
['test3','test3','test3'],
['test4','test4','test4'],
]
context={}
context['available_trackers'] = available_trackers
context['google_api_key'] = 'AIzaSyDqdNQy_aqDBPsc1hWpYaZlqZrSAiu1K8U'

def index(request):
    return render(request, 'index.html')

def login(request):
    return render(request, 'login.html')

def logout(request):
    return render(request, 'logout.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    context = {'form' : form}
    return render(request, 'register.html', context)

def overview(request):
    return render(request, 'overview.html', context)

def reports(request):
    return render(request, 'reports.html')

def alerts(request):

    return render(request, 'alerts.html')

def manage(request):
    return render(request, 'manage.html')

def search(request):
    return render(request, 'search.html', context)

def settings(request):
    return render(request, 'settings.html')

def chat(request):
    return render(request, 'chat.html')
